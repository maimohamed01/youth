import { Injectable } from '@angular/core';
import { CanActivate,Router,UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import {UserServiceService} from './services/user-service.service'

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(private router:Router,private UserServiceService:UserServiceService){}
  canActivate():boolean{
    if(this.UserServiceService.loggedIn()){
      return true;
    }else{
      this.router.navigate(['/login'])
      return false
    }
  }
}
