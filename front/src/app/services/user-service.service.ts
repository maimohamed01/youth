import { Injectable } from '@angular/core';
import {Observable} from 'rxjs'
import {HttpClient} from '@angular/common/http'
@Injectable({
  providedIn: 'root'
})
export class UserServiceService {
  constructor(private http:HttpClient) { }
  getTest(): Observable<any>{
    return this.http.get<any>('http://localhost:8083/api/auth');
  }
  loggedIn(){
    return !!localStorage.getItem('token');
  }  
}
