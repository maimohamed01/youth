import {HTTP_INTERCEPTORS} from '@angular/common/http'
import {JwtService} from './jwt.service'
import {ErrorService} from './error.service'

//httpInterceptorProvidersشايل الملفان الخاص بالركوست والخاص بالرسبونس واللى المفروض بعرف بيهم لمشروع ان فية انترسبتور هنا عن طرق ان بضيف المتغير دة فى ال
//app.module
export const httpInterceptorProviders = [
 {provide:HTTP_INTERCEPTORS , useClass:JwtService , multi:true},
 {provide:HTTP_INTERCEPTORS , useClass:ErrorService , multi:true}
];
