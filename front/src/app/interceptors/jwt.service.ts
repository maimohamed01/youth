import { Injectable,Injector } from '@angular/core';
import {getToken} from './getToken.service'
//requestدا خاص بالركوست بيبقى معاة التوكين اللى بيقرنها بالقيمة الموجودة عند السرفير ولو نفسها بيرجع الرسبونس لو لا بيروح لملف الايرور ودا خاص بالرسبيونس عشان يعمل هاند ل للايرور
//interceptorمعترض بيعترض الركوست والرسبونس عن طريق ان بدى للركوست هيدر الهيدر دة اللى بيقبى شايل التوكن
import { HttpInterceptor, HttpRequest,HttpHandler} from '@angular/common/http';
import { $ } from 'protractor';
const apiUrl="http://localhost:8083/api/auth";
@Injectable({
  providedIn: 'root'
})
@Injectable()
export class JwtService implements HttpInterceptor {
  constructor(private injector:Injector) { }
  //دى الmain function 
  // بتشوف الركوست او الرسبونس
  intercept(req:HttpRequest<unknown>,next:HttpHandler){
    console.log("test intercept")
    let authService = this.injector.get(getToken)
    // if(req.url.startsWith(apiUrl)){
    req=req.clone({
      setHeaders: {
        Authorization :`Bearer ${authService.getToken()}`
      }
    })
  // }
    return next.handle(req);
  }


}
