import { Injectable } from '@angular/core';
import {Observable, throwError} from 'rxjs'
import {catchError} from 'rxjs/operators'
import {Router} from '@angular/router'
import { HttpInterceptor, HttpRequest,HttpHandler} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})

@Injectable()
export class ErrorService implements HttpInterceptor {

  constructor(private router:Router) { }
  intercept(req:HttpRequest<unknown>,next:HttpHandler){
    return next.handle(req).pipe(
      catchError(err => {
        console.log(err);
        //here you can handle your errors
       // err.status === 401
        if([401,403].indexOf(err.status) !== -1){
          console.log('error from interceptor');
          this.router.navigate(['./login'])
        }
        return throwError(err);
      })
    );
  }
}
