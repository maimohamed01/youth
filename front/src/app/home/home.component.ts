import { Component, OnInit } from '@angular/core';
import {UserServiceService} from '../services/user-service.service'
import { OwlOptions } from 'ngx-owl-carousel-o';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  constructor(private user:UserServiceService ) { }
   userContainer:any;
  ngOnInit() {
    // this.user.getAllRequest().subscribe(i => {
    //     this.userContainer = i;
    // },err => {})
  }
  cards= [
    { photo: '../../assets/images/7.webp', title: 'Our MRMI', description: 'Mohammed Bin Rashid Al Maktoum Global Initiatives was inaugurated on 4 October 2015. ' },
    { photo: '../../assets/images/3.webp', title: 'Our Message', description: 'As a young Foundation, we are inspired by our young nation whose achievements have surpassed.' },
    { photo: '../../assets/images/6.webp', title: 'Our Vision', description: 'VisionAl Jalila Foundation is a global philanthropic organisation achievements have surpassed.' },
  ]
  customOptions: OwlOptions = {
    loop: true,
    mouseDrag: false,
    touchDrag: false,
    pullDrag: false,
    dots: false,
    autoplay:true,
    navSpeed: 4000,
    navText: ['', ''],
    responsive: {
      0: {
        items: 1
      },
      400: {
        items: 3
      },
      740: {
        items: 3
      },
      940: {
        items: 4
      }
    },
    nav: false
  }
}


