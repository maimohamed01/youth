import { Component, OnInit, ElementRef } from '@angular/core';
import {UserServiceService} from '../services/user-service.service'
import {Router} from '@angular/router';
import { OwlOptions } from 'ngx-owl-carousel-o';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor(private elementref:ElementRef,private UserServiceService:UserServiceService
              ,private router:Router) { }
  user={}
  testLogin(){
 this.UserServiceService.getTest().subscribe(res => {
   console.log(res.token)
   localStorage.setItem('token',res.token)
   this.router.navigate([''])
 },err =>{console.log("error")})
  }
  customOptions: OwlOptions = {
    loop: true,
    mouseDrag: true,
    touchDrag: true,
    pullDrag: true,
    dots: false,
    navSpeed: 700,
    autoplay: true,
    nav:false,
    responsive: {
      0: {
        items: 1
      }
    }
  }
  ngOnInit() {}
  

}
