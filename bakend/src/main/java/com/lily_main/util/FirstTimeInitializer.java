package com.lily_main.util;

import com.lily_main.model.AppUser;
import com.lily_main.security.UserService;
import org.apache.juli.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import com.lily_main.repository.userRepository;
import org.apache.juli.logging.Log;
import org.springframework.stereotype.Component;
//الكلاس دة بيتنفذ اول لما الرن
@Component
public class FirstTimeInitializer implements CommandLineRunner {
   private  final Log logger= LogFactory.getLog(FirstTimeInitializer.class);
    @Autowired
    private userRepository userRepository;
    @Autowired
    private UserService userService;
    @Override
    public void run(String... strings) throws Exception {
          if(userService.findAll().isEmpty()){
              logger.info("No Users Accounts found. Creating some users");
              AppUser user=new AppUser("nnnn","jjj");
              userService.save(user);
          }
    }
}
