package com.lily_main.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import com.lily_main.model.signInRequest;
import com.lily_main.security.TokenUtil;
import com.lily_main.security.UserService;
import org.springframework.security.authentication.AuthenticationManager;
import com.lily_main.model.signInRequest;
import com.lily_main.model.*;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;

@RestController
@RequestMapping(value = "/api/auth")
public class user {

    @Autowired
    private TokenUtil tokenUtil;

    @Autowired
    private UserService userService;

    @Autowired
    private AuthenticationManager authenticationManager;
    @CrossOrigin(origins = "http://localhost:4200")
    @GetMapping(value = {"","/"})
    public JwtResponse signIn(@RequestBody signInRequest signInRequest) {

//        final Authentication authentication = authenticationManager.authenticate(
//                new UsernamePasswordAuthenticationToken(signInRequest.getUsername(), signInRequest.getPass())
//        );
//
//        SecurityContextHolder.getContext().setAuthentication(authentication);

        UserDetails userDetails = userService.loadUserByUsername(signInRequest.getUsername());
        String token = tokenUtil.generateToken(userDetails);
        JwtResponse response = new JwtResponse(token);
        return response;
    }
}
