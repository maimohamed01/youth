package com.lily_main.model;

import org.springframework.data.mongodb.core.mapping.Document;

public class signInRequest {
    private String username;
    private String pass;
    public signInRequest(){}
    public signInRequest(String username,String pass){
        this.username=username;
        this.pass=pass;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }
}
