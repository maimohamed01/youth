package com.lily_main.repository;

import com.lily_main.model.AppUser;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;
import com.lily_main.model.signInRequest;

@Repository
public interface userRepository extends MongoRepository<AppUser,String> {
    AppUser findByEmail(String email);
}
