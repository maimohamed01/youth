package com.lily_main.security;

import com.lily_main.model.AppUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.SecurityProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import com.lily_main.repository.userRepository;

import java.util.*;
import org.springframework.data.crossstore.ChangeSetPersister.NotFoundException;
@Service
public class UserService implements UserDetailsService {
    @Autowired
  private userRepository userRepository;
    @Bean
   private PasswordEncoder passwordEncoder(){
       return new BCryptPasswordEncoder();
   }


    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
//        return new User("mmm",passwordEncoder().encode("bbbb"), AuthorityUtils.NO_AUTHORITIES);
        AppUser user=userRepository.findByEmail(username);
        if(user==null){
//            throw new NotFoundException("User not found");
        }
            return user;
    }
    public void save(AppUser user){
        user.setPassword(passwordEncoder().encode(user.getPassword()));
        this.userRepository.save(user);
    }
    public List<AppUser> findAll(){
        return userRepository.findAll();
    }
}
