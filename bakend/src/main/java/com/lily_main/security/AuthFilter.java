package com.lily_main.security;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.web.filter.OncePerRequestFilter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
//بيتنفذ قبل الركوست مرة واحدة بس
public class AuthFilter extends OncePerRequestFilter {
    @Value("${auth.header}")
    private String TOKEN_header;
    @Autowired
    private TokenUtil tokenUtil;
    @Autowired
    private UserService userService;
    @Override
    protected void doFilterInternal(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, FilterChain filterChain) throws ServletException, IOException {
        //get token from header
        //make sure its valid
      final String header=httpServletRequest.getHeader(TOKEN_header);
      final SecurityContext securityContext= SecurityContextHolder.getContext();
      if(header !=null && securityContext.getAuthentication() !=null){
        String token=header.substring("@Bearer".length());//عشان يحذف كلمة brearer من ال token
          String username=tokenUtil.getUserNameFromToken(token);
          if(username != null){
        UserDetails userDetails=userService.loadUserByUsername(username);
          if(tokenUtil.isTokenValid(token,userDetails)){
              UsernamePasswordAuthenticationToken authenticationToken=new UsernamePasswordAuthenticationToken(userDetails,null,userDetails.getAuthorities());
              authenticationToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(httpServletRequest));
              SecurityContextHolder.getContext().setAuthentication(authenticationToken);
          }}
      }
      filterChain.doFilter(httpServletRequest,httpServletResponse);
    }
}
