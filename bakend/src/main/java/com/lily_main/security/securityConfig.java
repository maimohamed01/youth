package com.lily_main.security;
import org.springframework.context.annotation.Bean;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
@EnableWebSecurity
public class securityConfig extends WebSecurityConfigurerAdapter {

    private final String[] PUBLICENDPOINTS={"/api/**"}; //دا الapi اللى اقدر اعملة access من غير ما دخل الusername والباصورد

    @Bean
    @Override
  protected AuthenticationManager authenticationManager() throws Exception{
      return super.authenticationManager();
  }
    @Bean
     AuthFilter authFilter(){
        return new AuthFilter();
    }

    @Override
   protected void configure(HttpSecurity http) throws  Exception{
        http.cors().and().csrf().disable().   //عشان مش محتاجين كوكيز
                sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS) //عشاان مفيش سيشن وهيستخدم الج دبيو تى
                .and().authorizeRequests() //مجتاج ال url يكون authorize ولا لا يعنى يدخل الuser name والباصورد
                .antMatchers(PUBLICENDPOINTS).permitAll()
                .anyRequest().authenticated(). //اى api غير اللى مديهولوة لازم يكون authenticated
                and().addFilterBefore(authFilter(), UsernamePasswordAuthenticationFilter.class);
   }
}
