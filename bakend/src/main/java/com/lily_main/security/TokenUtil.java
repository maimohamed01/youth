package com.lily_main.security;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;
import io.jsonwebtoken.Claims;
import com.lily_main.model.signInRequest;

import java.util.*;

import static io.jsonwebtoken.SignatureAlgorithm.ES256;

//الكلاس دة عشان يعمل generate لل token
@Component
public class TokenUtil {
    private final String CLAIMS_SUBJECT="sub";//عشان اتعرف على اليوزر
    private final String CLAIMS_CREATED="created";
    @Value("${auth.expiration}") //دا متغير متعرف فى الresource
    private long TOKEN_VALIDITY=604800L; // 7day
    @Value("${auth.secret}")
    private String TOKEN_SECRET;

    public String generateToken(UserDetails userDetails){
//    claim
//    expretion
//    sign
//    compact
        Map<String ,Object> claims=new HashMap<>();
        claims.put(CLAIMS_SUBJECT,"kkkkkkkk");
        claims.put(CLAIMS_CREATED,new Date());

        return Jwts.builder()
                .setClaims(claims)
                .setExpiration(generateExpirationDate())
//                .signWith(ES256,TOKEN_SECRET)
                .compact();
    }

    private  Date generateExpirationDate(){
        return new Date(System.currentTimeMillis()+TOKEN_VALIDITY*1000); //هاتى الوقت الحالى وضيف عليها القيمة الموجودة فى المتغير دة مضروبة فى 1000 عشان يحولها لثانية
    }

    public String getUserNameFromToken(String token){
        try{
           Claims claims=getClaims(token);
           return claims.getSubject();
        }catch (Exception e){
               return null;
        }
    }

   public boolean isTokenValid(String token,UserDetails userDetails) {
        String username=getUserNameFromToken(token);
        return (username.equals(userDetails.getUsername())&& !isTokenExpired(token));
    }
    private boolean isTokenExpired(String token){
          Date expiration=getClaims(token).getExpiration();
          return expiration.before(new Date());
    }

    private Claims getClaims(String token){
        Claims claims;
        try{
            claims=Jwts.parser().setSigningKey(TOKEN_SECRET).parseClaimsJws(token)
                    .getBody();
        }catch (Exception e){
            return null;
        }
        return claims;

    }
}